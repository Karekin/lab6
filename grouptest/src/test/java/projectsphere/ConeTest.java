package projectsphere;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.lang.IllegalArgumentException;
import java.lang.AssertionError;

import java.lang.IllegalArgumentException;
import java.lang.Math;
import org.junit.Test;

import java.beans.Transient;
import java.lang.Math;
/**
 * This is a test class that will assure the Cone class returns the correct values
 * and that it only allows correct values in its constructor
 * 
 * @author Karekin Kiyici
 * @version 9/15/2022
 */
public class ConeTest {
    
    @Test
    /**
     * Test to make sure the values of the Cone class are set correctly
     */
    public void test_coneCreation_given_correct_values(){
        Cone cone1 = new Cone(5.5, 9);
        assertEquals(5.5,cone1.getHeight(),0);
        assertEquals(9,cone1.getRadius(),0);
    }

    @Test(expected=IllegalArgumentException.class)
    public void test_coneCreation_given_negative_values(){
        try {
            Cone cone1 = new Cone(-5.5,-9);
            fail();
        }catch (AssertionError exception){
         String expectedMessage = "Please provide a positive number!";
         String actualMessage = exception.getMessage();
         assertTrue(actualMessage.contains(expectedMessage));
        }
    }

    @Test
    public void test_volumeMethod_given_correct_values(){
        Cone cone1 = new Cone(4,8);
        double volume = cone1.getVolume();
        assertEquals(Math.PI * Math.pow(8, 2) * (4.0/3), volume, 0.1);
    }

    @Test
    public void test_surfaceAreaMethod_given_correct_values(){
        Cone cone1 = new Cone(2,5);
        double surfaceArea = cone1.getSurfaceArea();
        assertEquals(Math.PI * 5 * (5 + Math.sqrt(2 * 2 + 5 * 5)), surfaceArea, 0);
    }
}
