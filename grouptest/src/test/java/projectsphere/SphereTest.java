package projectsphere;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.lang.IllegalArgumentException;

import org.junit.Test;

/**
 * This is the testing class for Sphere.java
 * 
 * @author Hernan Mathias Farina Forster
 * @version 9/15/2022
 */

public class SphereTest {
    @Test
    public void test_get_given_volume_correct_output() {
        Sphere sphere = new Sphere(2);
        double actual_output = Math.PI * Math.pow(sphere.getRadius(),3);
        assertEquals(Math.PI*8, actual_output, 0);
    }
    @Test
    public void test_get_given_area_correct_output() {
        Sphere sphere = new Sphere(2);
        double actual_output = 4 * Math.PI * Math.pow(sphere.getRadius(),2);
        assertEquals(4 * Math.PI * 4, actual_output, 0.1);
    }
    @Test
    public void test_get_correct_radius() {
        Sphere sphere = new Sphere(2);
        double actual_output = sphere.getRadius();
        assertEquals(2, actual_output, 0);
    }
    @Test(expected=IllegalArgumentException.class)
    public void test_if_negative_number_input() {
  
            try {
                Sphere sphere = new Sphere(-1);
                fail();
            }catch (AssertionError exception){
                String expectedMessage = "Radius must be positive";
                String actualMessage = exception.getMessage();
                assertEquals(expectedMessage,actualMessage);
        }
    }
}