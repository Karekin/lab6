package projectsphere;
import java.lang.Math;

/**
 * This is the class having the Cone properties.
 * 
 * @author Hernan Mathias Farina Forster
 * @version 9/15/2022
 */

public class Cone {
    private double height;
    private double radius;

    public Cone(double height, double radius) {
        this.height = height;
        this.radius = radius;

        if(this.height < 0 || this.radius < 0) {
            throw new IllegalArgumentException("Please provide a positive number!");
        } 
    }
    public double getHeight() {
        return this.height;
    }
    public double getRadius() {
        return this.radius;
    } //Formula: pi * radius to the power of 2 + height
    public double getVolume() {
        return Math.PI * Math.pow(this.radius, 2) * (this.height/3);
    } //Formula: pi * radius * (radius + sqrt(height + radius(both to the power of 2)))
    public double getSurfaceArea() {
        return  Math.PI * this.radius * (this.radius + Math.sqrt(this.height * this.height + this.radius * this.radius));
    }
}
