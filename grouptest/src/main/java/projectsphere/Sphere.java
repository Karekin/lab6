package projectsphere;
import java.lang.Math;

/**
 *  This is a class that creates sphere objects and computes them
 * 
 * @author Karekin Kiyici
 * @version 9/15/2022
 */

public class Sphere{
    
    private double radius;
    /**
     * Constructor method that intakes a double and validates
     * to make sure the value isn't negative.
     * @param inRadius
     */
    public Sphere(double inRadius){
        
        if(inRadius < 0){
            throw new IllegalArgumentException("Radius must be positive");
        }else {
            this.radius = inRadius;
        }
    }
    /**
     * 
     * @return Returns the radius field
     */
    public double getRadius(){
        return this.radius;
    }
    /**
     * Computes the volume formula for spheres and returns the result
     * @return
     */
    public double getVolume(){
        return (Math.PI * Math.pow(this.radius, 3));
    }
    /**
     * Computes the surface area formula for spheres and returns the result
     * @return
     */
    public double getSurfaceArea(){
        return (4 * Math.PI * Math.pow(this.radius, 2));
    }
}